<?php

namespace Taip\Policies;

use Taip\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the logged user can view the user.
     *
     * @param  \Taip\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        //
    }

    /**
     * Determine whether the logged user can create users.
     *
     * @param  \Taip\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->role === 255);
    }

    /**
     * Determine whether the logged user can update the user.
     *
     * @param  \Taip\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return ($user->role === 255);
    }

    /**
     * Determine whether the logged user can delete the user.
     *
     * @param  \Taip\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return ($user->role === 255);
    }
}
