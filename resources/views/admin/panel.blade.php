@extends('master')
@section('title')
    Panel administracyjny
@endsection
@section('content')
    <div class="container mtb">
        @if (Session::has('admin_action_performed'))
            <div class="alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('admin_action_performed') }}
            </div>
        @endif
        <div class="row">

            <div class="col-lg-7">
                <h4>Artykuły czekające na zatwierdzenie</h4>
                <div class="hline"></div>
            @foreach ($articles as $article)
                <!-- Articles -->
                    <div>
                        <a href="{{ URL::route('articles.show', ['id' => $article->id]) }}">
                            <h5 class="ctitle">{{ $article->title }}</h5>
                        </a>
                        <a class="btn btn-theme pull-right" style="padding: 3px 10px; margin: 0 4px;" href="{{ URL::route('admin.panel') }}"
                           onclick="event.preventDefault();
                                    document.getElementById('admin_publish_form').submit();">
                            <i class="fa fa-check" aria-hidden="true"></i> Opublikuj
                        </a>
                        <a class="btn btn-theme pull-right" style="padding: 3px 10px; margin: 0 4px;" href="{{ URL::route('admin.panel') }}"
                           onclick="event.preventDefault();
                                    document.getElementById('admin_delete_form').submit();">
                            <i class="fa fa-trash-o" aria-hidden="true"></i> Usuń
                        </a>

                        {!! Form::open(['route' => ['admin.panel.publish', $article->id], 'method' => 'PATCH', 'style' => 'display: none;', 'id' => 'admin_publish_form']) !!}
                        {!! Form::close() !!}
                        {!! Form::open(['route' => ['articles.destroy', $article->id], 'method' => 'DELETE', 'style' => 'display: none;', 'id' => 'admin_delete_form']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div>
                        <ul class="meta-post">
                            <li><i class="fa fa-clock-o"></i> <span>{{ date('F d, Y G:i', strtotime($article->created_at)) }}</span></li>
                            <li><i class="fa fa-user"></i><a href="#"> {{ $article->user->name }}</a></li>
                        </ul>
                    </div>

                    <p>{{ $article->intro_content }}</p>
                <div class="spacing"></div>

                @endforeach
            </div> <!--/col-lg-8 -->


            <!-- Users -->
            <div class="col-lg-5">
                <h4>Ostatni użytkownicy</h4>
                <div class="hline"></div>
                @foreach ($users as $user)
                    <p>
                        <a href="#">
                            <i class="fa fa-angle-right"></i> {{ $user->name }}
                        </a>
                        <a class="btn btn-theme pull-right" style="padding: 3px 10px; margin: 0;" href="{{ URL::route('admin.panel') }}"
                           onclick="event.preventDefault();
                                    document.getElementById('admin_promote_form').submit();">
                            <i class="fa fa-level-up" aria-hidden="true"></i> Zwiększ uprawnienia
                        </a>
                        {!! Form::open(['route' => ['admin.panel.promote', $user->id], 'method' => 'PATCH', 'style' => 'display: none;', 'id' => 'admin_promote_form']) !!}
                        {!! Form::close() !!}
                    </p>
                @endforeach

            </div>
        </div> <!--/row -->
    </div> <!--/container -->
@endsection