@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="float: none; list-style: none; margin: 0; padding: 0;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('name', 'Nazwa') !!}
    </div>
    <div class="col-md-9">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź nazwę', 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('short_name', 'Krótka nazwa') !!}
    </div>
    <div class="col-md-9">
        {!! Form::text('short_name', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź krótką nazwę', 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('description', 'Opis') !!}
    </div>
    <div class="col-md-9">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź opis kategorii', 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-9 col-md-offset-3">
        {!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i> ' . $sendText, ['type' => 'submit', 'class' => 'btn btn-theme']) !!}
    </div>
</div>