@extends('master')
@section('title')
    Dodawanie kategorii
@endsection
@section('content')
    <div class="container mtb">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="card">
                <div class="panel-body">
                    <!-- Formularz -->
                    {!! Form::open(['route' => 'categories.index', 'class' => 'form-horizontal']) !!}

                        @include('categories.form', ['sendText' => 'Dodaj kategorię'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection