<?php

namespace Taip;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'content',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'commentable_id', 'commentable_type', 'user',
    ];

    /**
     * The relations to eager load on every query.
     * https://laravel.com/docs/5.4/eloquent-relationships#eager-loading
     *
     * @var array
     */
    protected $with = [
        'user',
    ];

    /**
     * Get comment author's name.
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Convert the model instance to an array.
     * This is called by toJson() method, so it isn't required to call it explicitly in Response::json().
     *
     * @return array
     */
    public function toArray() {
        $data = parent::toArray();

        if ($this->user) {
            $data['user_name'] = $this->user->name;
        }
        else {
            $data['user_name'] = null;
        }

        return $data;
    }
}
