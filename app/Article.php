<?php

namespace Taip;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'category_id', 'intro_content', 'full_content', 'img',
    ];

    /**
     * Get article's author.
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get article's category.
     */
    public function category() {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get all of the article's comments.
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
