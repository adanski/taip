<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', //function() {
    //return view('welcome');
    'ArticlesController@index'
)->name('home');
Route::get('home', 'HomeController@index');

/*
 * Authentication
 */
Auth::routes();

/*
 * Static pages
 */
Route::get('contact', 'PagesController@contact')->name('contact');
Route::post('contact', 'PagesController@contactMessage')->name('contact.message');
Route::get('about', 'PagesController@about')->name('about');

/*
 * App modules' pages
 */
// users
Route::resource('users', 'UsersController');

// categories
Route::resource('categories', 'CategoriesController');

// articles
Route::resource('articles/{article}/comments', 'CommentsController', ['except' => [
    'create', 'show', 'edit',
]]);
Route::get('articles/popular', 'ArticlesController@showPopular')->name('articles.showPopular');
Route::get('articles/own', 'ArticlesController@indexOwn')->name('articles.indexOwn');
Route::patch('articles/{article}/publish', 'AdminController@update')->name('articles.publish');
Route::resource('articles', 'ArticlesController');

// admin
Route::get('admin', 'AdminController@index')->name('admin.panel');
//Route::patch('admin/articles/{article}', 'AdminController@update')->name('admin.panel.publish');
//Route::patch('admin/users/{user}', 'AdminController@promote')->name('admin.panel.promote');

