<div id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <h4>Bądź na bieżąco</h4>
                <div class="hline-g"></div>
                <p>
                    Zapisz się na nasz newsletter.
                </p>
                <form id="newsletter_signup">
                    <div class="input-group">
                        <input id="newsletter_email" class="form-control" type="email"
                               placeholder="Wprowadź adres e-mail">
                        <span class="input-group-btn">
		 						<button type="submit" class="btn btn-default">Zapisz</button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="col-lg-3">
                <h4>Przydatne linki</h4>
                <div class="hline-g"></div>
                <ul>
                    <li><a href="{{ URL::route('contact') }}">Zgłoś błąd</a></li>
                    <li><a href="https://europa.eu/european-union/abouteuropa/cookies_pl">Informacja o cookies</a></li>
                    <li><a href="{{ URL::route('contact') }}">Reklama</a></li>
                </ul>
            </div>

            <div class="col-lg-6">
                <h4>O nas</h4>
                <div class="hline-g"></div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque
                    sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac
                    diam. Quisque semper justo at risus.
                </p>
            </div>
        </div><!-- /row -->
        <div class="row">
            <div id="footer-end">
                <p>
                    Copyright © 2017 TAIP {{ $copyrightOwner }}
                </p>
                <ul class="social-network">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-reddit-alien"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!-- /container -->
</div><!-- /footerwrap -->