<h4>Szukaj</h4>
<div class="hline"></div>
<p>
    <br/>
    <input type="text" class="form-control" placeholder="Wpisz kryteria">
</p>

<div class="spacing"></div>

<h4>Popularne kategorie</h4>
<div class="hline"></div>
@php
    $categories = DB::table('categories')
                  ->join('articles', 'articles.category_id', '=', 'categories.id')
                  ->select(DB::raw('categories.name as name, count(articles.id) as articles_count'))
                  ->groupBy('categories.name')
                    ->havingRaw('count(articles.id)')
                    ->limit(6)->get();
    foreach ($categories as $category) {
        echo '<p><a href="#"><i class="fa fa-angle-right"></i> ' . $category->name
        . '</a> <span class="badge badge-theme pull-right">'
        . $category->articles_count . '</span></p>';
    }

@endphp

<div class="spacing"></div>

<h4>Ostatnie komentarze</h4>
<div class="hline"></div>
<ul class="popular-posts">
    @php
        $comments = DB::table('comments')
                    ->join('users', 'comments.user_id', '=', 'users.id')
                    ->select('users.name', 'comments.created_at', 'comments.content')
                    ->orderBy('comments.created_at', 'desc')
                    ->limit(3)
                    ->get();
        foreach ($comments as $comment) {
            echo '<li><a href="#"><img src="' . URL::asset('img/avatar.png') . '" alt="' . $comment->name
            . '" class="img-responsive img-smallie"/></a>
            <p><a href="#">' . mb_strimwidth($comment->content, 0, 86, "...") . '</a></p>
            <em>' . $comment->created_at . '</em></li>';
        }
    @endphp
</ul>

<div class="spacing"></div>

<h4>Popularne tagi</h4>
<div class="hline"></div>
<p>
    <a class="btn btn-theme" href="#" role="button">Design</a>
    <a class="btn btn-theme" href="#" role="button">Wordpress</a>
    <a class="btn btn-theme" href="#" role="button">Flat</a>
    <a class="btn btn-theme" href="#" role="button">Modern</a>
    <a class="btn btn-theme" href="#" role="button">Wallpaper</a>
    <a class="btn btn-theme" href="#" role="button">HTML5</a>
    <a class="btn btn-theme" href="#" role="button">Pre-processor</a>
    <a class="btn btn-theme" href="#" role="button">Developer</a>
    <a class="btn btn-theme" href="#" role="button">Windows</a>
    <a class="btn btn-theme" href="#" role="button">Phothosop</a>
    <a class="btn btn-theme" href="#" role="button">UX</a>
    <a class="btn btn-theme" href="#" role="button">Interface</a>
    <a class="btn btn-theme" href="#" role="button">UI</a>
    <a class="btn btn-theme" href="#" role="button">Blog</a>
</p>
