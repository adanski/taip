@extends('master')
@section('title')
    Edycja kategorii
@endsection
@section('content')
    <div class="container mtb">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="card">
                    <div class="panel-body">
                        <!-- Formularz -->
                        {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}

                            @include('categories.form', ['sendText' => 'Zapisz zmiany'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection