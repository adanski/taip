@extends('master')
@section('title')
    Najnowsze artykuły
@endsection
@section('content')
    <div class="container mtb">
        @if (Session::has('article_created_deleted'))
            <div class="alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('article_created_deleted') }}
            </div>
        @endif
        <div class="row">

            <!-- BLOG POSTS LIST -->
            <div class="col-lg-8">

                @foreach ($articles as $article)
                <!-- Article brief -->
                <p><img class="img-responsive" src="{{ URL::asset(Storage::url($article->img)) }}"></p>
                <a href="{{ URL::route('articles.show', ['id' => $article->id]) }}"><h3 class="ctitle">{{ $article->title }}</h3></a>
                <div class="bottom-article">
                    <ul class="meta-post">
                        <li><i class="fa fa-clock-o"></i> <span>{{ date('F d, Y G:i', strtotime($article->created_at)) }}</span></li>
                        <li><i class="fa fa-user"></i><a href="#"> {{ $article->user->name }}</a></li>
                        {{--<li><i class="fa fa-comments"></i><a href="{{ URL::route('articles.show', ['id' => $article->id]) }}#comments_section">3 komentarze</a></li>--}}
                        <li><i class="fa fa-tags"></i><a href="#">{{ $article->category->name }}</a></li>
                    </ul>
                </div>
                <p>{{ $article->intro_content }}</p>
                <p><a href="{{ URL::route('articles.show', ['id' => $article->id]) }}">Czytaj dalej</a></p>
                <div class="hline"></div>

                <div class="spacing"></div>

                @endforeach
            </div><!--/col-lg-8 -->


            <!-- SIDEBAR -->
            <div class="col-lg-4">
                @include ('layouts.sidebar')
            </div>
        </div><!--/row -->
    </div><!--/container -->
@endsection