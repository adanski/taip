<?php

namespace Taip\Policies;

use Taip\User;
use Taip\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the article.
     *
     * @param  \Taip\User  $user
     * @param  \Taip\Article  $article
     * @return mixed
     */
    public function view(User $user, Article $article)
    {
        //
    }

    /**
     * Determine whether the user can create articles.
     *
     * @param  \Taip\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the article.
     *
     * @param  \Taip\User  $user
     * @param  \Taip\Article  $article
     * @return mixed
     */
    public function update(User $user, Article $article)
    {
        return (($user->id === $article->user_id) || ($user->role === 255));
    }

    /**
     * Determine whether the user can delete the article.
     *
     * @param  \Taip\User  $user
     * @param  \Taip\Article  $article
     * @return mixed
     */
    public function delete(User $user, Article $article)
    {
        return ($user->role === 255);
    }
}
