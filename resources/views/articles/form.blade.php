@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="float: none; list-style: none; margin: 0; padding: 0;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('title', 'Tytuł') !!}
    </div>
    <div class="col-md-9">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź tytuł', 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('intro_content', 'Wstęp') !!}
    </div>
    <div class="col-md-9">
        {!! Form::textarea('intro_content', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź wstęp artykułu (wyświetlany na stronie głównej)', 'rows' => '4', 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('full_content', 'Treść') !!}
    </div>
    <div class="col-md-9">
        {!! Form::textarea('full_content', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź pozostałą treść artykułu (wyświetlany na stronie danego artykułu)', 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('category_id', 'Kategoria') !!}
    </div>
    <div class="col-md-9">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'placeholder' => 'Wybierz kategorię', 'required']) !!}
    </div>
</div>

@if ($sendText == 'Dodaj artykuł')
<div class="form-group">
    <div class="col-md-3 control-label">
        {!! Form::label('img', 'Obrazek') !!}
    </div>
    <div class="col-md-9">
        {!! Form::file('img', ['required']) !!}
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-md-9 col-md-offset-3">
        {!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i> ' . $sendText, ['type' => 'submit', 'class' => 'btn btn-theme']) !!}
    </div>
</div>