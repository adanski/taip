@extends('master')
@section('title')
    Logowanie
@endsection
@section('content')
    <div class="container mtb">
        <div class="row row-centered">
            <div class="col-md-5">
                <div class="card">
                    <div class="panel-body">
                        {!! Form::open(['route' => 'login', 'class' => 'form-horizontal']) !!}

                        @if ($errors->has('name') || $errors->has('password'))
                            <div class="alert alert-danger">
                                <ul style="float: none; list-style: none; margin: 0; padding: 0;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            {!! Form::label('name', 'Nazwa użytkownika', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź nazwę użytkownika', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Hasło', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <!--<input type="password" class="form-control" name="password" id="password"  placeholder="Wprowadź hasło"/>-->
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Wprowadź hasło', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox pull-left">
                                <label>
                                    <input type="checkbox" name="remember"> Zapamiętaj mnie
                                </label>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-link" href="{{ url('/password/reset') }}" style="padding-right: 0;">
                                    Zapomniałeś hasła?
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Zaloguj</button>
                        </div>
                        <div class="login-register">
                            Nie masz jeszcze konta? <a href="{{ URL::route('register') }}">Zarejestruj się</a>, to zajmie mniej niż minutę.
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection