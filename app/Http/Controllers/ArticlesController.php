<?php

namespace Taip\Http\Controllers;

use Illuminate\Http\Request;
use Debugbar;
use Exception;
use Session;
use Taip\Article;
use Taip\Category;
use Taip\Comment;
use Taip\Http\Requests\CreateArticleRequest;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller {
    public function __construct() {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update', 'destroy']);
    }

    /**
     * Show article list (by date, descending).
     */
    public function index() {
        // https://laravel.com/docs/5.4/eloquent-relationships#eager-loading
        $articles = Article::with('user', 'category')->latest()->where('published', '=', true)->get();
        return view('articles.index', compact('articles'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexOwn() {
        $articles = Article::with('user', 'category')->latest()->where('user_id', '=', Auth::user()->id)->get();
        return view('articles.index', compact('articles'));
    }

    /*public function indexCategory(Category $category) {
        $articles = Article::latest()->where('category_id', '=', $category->id)->get();
        return view('articles.index', ['articles' => $articles]);
    }*/

    /**
     * Show the specified article.
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Article $article) {
        $commentsCount = $article->comments()->count();
        return view('articles.show', compact('article', 'commentsCount'));
    }

    /**
     * Show the form for adding an article.
     */
    public function create() {
        // ::lists is deprecated, use ::pluck instead
        $categories = Category::pluck('name', 'id');
        return view('articles.create')->with('categories', $categories);
    }

    /**
     * Store newly created article in storage.
     * TODO: proper form validation
     *
     * @param CreateArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateArticleRequest $request) {
        // TODO: store img under public in storage
        $img = $request->file('img')->store('imgs');
        $article = new Article($request->all());
        $article->img = $img;
        try {
            Auth::user()->articles()->save($article);
        }
        catch (Exception $e) {
            Debugbar::addThrowable($e);
            return redirect()->back();
        }

        Session::flash('article_created_deleted', 'Artykuł został dodany.');
        return redirect()->route('articles.index');
    }

    /**
     * Show the form for editing an article.
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Article $article) {
        $categories = Category::pluck('name', 'id');
        return view('articles.edit', compact('categories', 'article'));
    }


    /**
     * Update the article in storage.
     *
     * @param CreateArticleRequest $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CreateArticleRequest $request, Article $article) {
        $article->update($request->all());
        Session::flash('article_edited', 'Artykuł zmieniony pomyślnie.');
        return redirect('articles/' . $article->id);
    }


    /**
     * Remove the article from storage.
     *
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Article $article) {
        $article->delete();
        Session::flash('article_created_deleted', 'Artykuł usunięty pomyślnie.');
        return redirect()->route('articles.index');
    }
}
