<?php

namespace Taip\Http\Controllers;

use Illuminate\Http\Request;
use Taip\Category;
use Session;
use Exception;
use Validator;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        $categories = Category::latest()->get();
        return view('categories.index', ['categories' => $categories]);
    }

    public function create() {
        return view('categories.create');
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'short_name' => 'required|max:255|alpha_dash',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('categories.create')
                ->withErrors($validator)
                ->withInput();
        }

        $category = new Category($request->all());
        try {
            $category->save();
        }
        catch (Exception $e) {
            Debugbar::addThrowable($e);
            return redirect()->back();
        }

        Session::flash('category_created_deleted', 'Kategoria została dodana.');
        return redirect()->route('categories.index');
    }
}
