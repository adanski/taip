@extends('master')
@section('title')
    Edycja artykułu
@endsection
@section('content')
    <div class="container mtb">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="card">
                    <div class="panel-body">
                        <!-- Formularz -->
                        {!! Form::model($article, ['route' => ['articles.update', $article->id], 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}

                            @include('articles.form', ['sendText' => 'Zapisz zmiany'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection