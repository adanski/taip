<?php

namespace Taip\Http\Controllers;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Taip\Article;
use Taip\Comment;
use Debugbar;
use Taip\Http\Requests\StoreOrUpdateCommentRequest;
use Validator;
use Response;
use Exception;

class CommentsController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index');
    }

    /**
     * Return comment list of specified resource in JSON.
     *
     * @param object $resource
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($resource) {
        $comments = $resource->comments;
        return response()->json($comments);
    }


    /**
     * Store newly created comment in storage.
     *
     * @param StoreOrUpdateCommentRequest $request
     * @param object $resource
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreOrUpdateCommentRequest $request, $resource) {
        if ($request->input('user_name') !== Auth::user()->name) {
            return Response::json('Unauthorized', 401);
        }
        $comment = new Comment($request->only(['parent_id', 'content']));
        $comment->user_id = Auth::id();

        try {
            $resource->comments()->save($comment);
        }
        catch (Exception $e) {
            Debugbar::addThrowable($e);
            return Response::json('Unprocessable Entity', 422);
        }
        return response()->json($comment);
    }

    /**
     * Update the comment in storage.
     *
     * @param StoreOrUpdateCommentRequest $request
     * @param object $resource
     * @param int $commentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreOrUpdateCommentRequest $request, $resource, $commentId) {
        if ($request->input('user_name') !== Auth::user()->name) {
            return Response::json('Unauthorized', 401);
        }

        try {
            $comment = Comment::findOrFail($commentId);
        }
        catch (Exception $e) {
            Debugbar::addThrowable($e);
            return Response::json('Unprocessable Entity', 422);
        }

        if (Auth::id() !== $comment->user->id) {
            return Response::json('Unauthorized', 401);
        }

        $comment->update($request->only(['content']));
        return response()->json($comment);
    }

    /**
     * Remove the comment from storage.
     *
     * @param object $resource
     * @param int $commentId
     * @return @return \Illuminate\Http\JsonResponse
     */
    public function destroy($resource, $commentId) {
        try {
            $comment = Comment::findOrFail($commentId);
        }
        catch (Exception $e) {
            Debugbar::addThrowable($e);
            return Response::json('Unprocessable Entity', 422);
        }

        $comment->delete();
    }
}
