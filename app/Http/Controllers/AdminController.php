<?php

namespace Taip\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Taip\Article;
use Taip\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['index']);
    }

    /**
     * Proof of concept admin controller
     */
    public function index() {
        // TODO: refactor ugly check
        if (\Auth::user()->role !== 255) {
            Session::flash('article_created_deleted', 'Nie masz wystarczających uprawnień do tej akcji.');
            return redirect()->route('articles.index');
        }
        $articles = Article::latest()->where('published', '=', false)->get();
        $users = User::latest()->where('role', '<>', 255)->get();
        return view('admin.panel', compact('articles', 'users'));
    }

    public function update($id) {
        if (\Auth::user()->role === 255) {
            $article = Article::findOrFail($id);
            $article->published = true;
            $article->save();
            Session::flash('admin_action_performed', 'Artykuł opublikowany pomyślnie.');
        }
        return redirect()->route('admin.panel');
    }

    public function promote($id) {
        if (\Auth::user()->role === 255) {
            $user = User::findOrFail($id);
            $user->role = 255;
            $user->save();
            Session::flash('admin_action_performed', 'Użytkownik pomyślnie ustanowiony adminem.');
        }
        return redirect('/admin');//->route('admin.panel');
    }
}
