<?php

namespace Taip\Http\Controllers;

use Illuminate\Http\Request;
use Taip\User;
use Debugbar;
use Response;
use Exception;
use Validator;

class UsersController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->only(['update', 'destroy']);
    }

    /**
     * Return user list of specified resource in JSON.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // TODO: validation, access policy etc.
        //$users = User::all();
        //return response()->json($users);
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
