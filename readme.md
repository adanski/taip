# This Ain't no Intractable PHP app

This is a simple blogging app based on Laravel 5.5. Work in progress.

## What does it offer?

- Users module (registration, authentication, authorization)
- Articles module (creating, editing, removing)
- AJAX-powered comments system (thanks to Viima's jquery-comments plugin)
- Categories module
- Proof of concept articles tagging
- Proof of concept admin panel


## TODOs

The code requires some minor cleanups and the whole system still lacks full English translation. Implementing a real admin panel and SimpleMDE Markdown Editor are the two priorities at the moment.