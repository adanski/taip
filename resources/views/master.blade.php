<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ $websiteTitle }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/jquery-comments.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="site-content">

    <!-- Fixed navbar -->
    <header class="header">
        @include('layouts.header')
    </header>

    <!-- *****************************************************************************************************************
     BLUE WRAP
     ***************************************************************************************************************** -->
    <article>

        {{--<div class="content-header">--}}
        <div id="blue">
            <div class="container">
                <div class="row">
                    <div class="content-header">
                        <h1>@yield('title')</h1>
                        @if (Route::currentRouteNamed('articles.show'))
                            {{--<div class="col-lg-12">--}}
                            @yield('article_path')
                            {{--</div>--}}
                        @endif

                    </div>
                </div><!-- /row -->
            </div> <!-- /container -->
        </div><!-- /blue -->

        @if (Route::currentRouteNamed('contact'))
            <div id="contactwrap"></div>
        @endif

        @yield('content')
    </article>
</div> <!-- /site-content -->
<!-- *****************************************************************************************************************
 FOOTER
 ***************************************************************************************************************** -->
<footer class="footer">
    @include('layouts.footer')
</footer>

<!-- Placed at the end of the document so the pages load faster -->
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>;
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/retina-1.1.0.js') }}"></script>
<script src="{{ URL::asset('js/jquery.hoverdir.js') }}"></script>
<script src="{{ URL::asset('js/jquery.hoverex.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ URL::asset('js/jquery.isotope.min.js') }}"></script>

@yield('customJs')

</body>
</html>
