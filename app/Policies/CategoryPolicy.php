<?php

namespace Taip\Policies;

use Taip\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return ($user->role === 255);
    }
}
