 <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::route('home') }}">TAIP</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="{{ Route::currentRouteNamed('home') ? 'active' : '' }}"><a href="{{ URL::route('home') }}">GŁÓWNA</a></li>
                    <li class="{{ Route::currentRouteNamed('about') ? 'active' : '' }}"><a href="{{ URL::route('about') }}">O NAS</a></li>
                    <li class="dropdown {{ (strpos(Route::currentRouteName(), 'articles.') !== false) || (strpos(Route::currentRouteName(), 'categories.') !== false) ? 'active' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ARTYKUŁY <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ URL::route('categories.index') }}">KATEGORIE</a></li>
                            {{--<li><a href="{{ URL::route('articles.showPopular') }}">NAJPOPULARNIEJSZE</a></li>--}}
                        </ul>
                    </li>
                    <li class="{{ Route::currentRouteNamed('contact') ? 'active' : '' }}"><a href="{{ URL::route('contact') }}">KONTAKT</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li class="{{ Route::currentRouteNamed('login') || Route::currentRouteNamed('register') ? 'active' : '' }}"><a href="{{ URL::route('login') }}"><i class="fa fa-sign-in"> </i> LOGOWANIE</a></li>
                    @else
                        <li class="dropdown {{ strpos(Route::currentRouteName(), 'admin.') !== false ? 'active' : '' }}">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding-top: 8px; padding-bottom: 8px;">
                                <img src="{{ URL::asset('img/avatar.png') }}" alt="{{ Auth::user()->name }}" style="height: 34px; width: 34px; margin-right: 6px" />
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ URL::route('articles.create') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        DODAJ ARTYKUŁ
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::route('articles.indexOwn') }}">
                                        <i class="fa fa-list-ol" aria-hidden="true"></i>
                                        MOJE ARTYKUŁY
                                    </a>
                                </li>
                                @if (Auth::user()->role === 255)
                                    <li>
                                        <a href="{{ URL::route('categories.create') }}">
                                            <i class="fa fa-plus" aria-hidden="true"></i> DODAJ KATEGORIĘ
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('admin.panel') }}">
                                            <i class="fa fa-cogs" aria-hidden="true"></i> PANEL ADMINISTRACYJNY
                                        </a>
                                    </li>
                                @endif
                                <li>
                                    <a href="{{ URL::route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        WYLOGUJ SIĘ
                                    </a>

                                    <form id="logout-form" action="{{ URL::route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->


        </div>
 </div>