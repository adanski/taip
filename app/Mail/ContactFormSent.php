<?php

namespace Taip\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class ContactFormSent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Contains filled form.
     *
     * @var Request
     */
    public $request;

    /**
     * Create a new message instance.
     *
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('layouts.email')
            ->subject('[Kontakt] ' . $this->request->input('subject'))
            ->from($this->request->input('email'), $this->request->input('name', 'Anonim'));
    }
}
