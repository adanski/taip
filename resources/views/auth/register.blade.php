@extends('master')
@section('title')
    Rejestracja
@endsection
@section('content')
    <div class="container mtb">
        <div class="row row-centered">
            <div class="col-md-5">
                <div class="card">
                    <div class="panel-body">
                        {!! Form::open(['route' => 'register', 'class' => 'form-horizontal']) !!}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul style="float: none; list-style: none; margin: 0; padding: 0;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="form-group">
                            {!! Form::label('name', 'Nazwa użytkownika', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź nazwę użytkownika', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Adres e-mail', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź e-mail', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Hasło', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Wprowadź hasło', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Potwierdź hasło', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Potwierdź hasło', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Zarejestruj</button>
                        </div>
                        <div class="login-register">
                            Masz już konto? <a href="{{ URL::route('login') }}">Zaloguj się</a>.
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection