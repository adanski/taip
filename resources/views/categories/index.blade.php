@extends('master')
@section('title')
    Lista kategorii
@endsection
@section('content')
    <div class="container mtb">
        @if (Session::has('category_created_deleted'))
            <div class="alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('category_created_deleted') }}
            </div>
        @endif
        <div class="row">

            <!-- BLOG POSTS LIST -->
            <div class="col-lg-8">

                @foreach ($categories as $category)
                <!-- Article brief -->
                <a href="#"><h3 class="ctitle">{{ $category->name }}</h3></a>
                <div class="bottom-article">
                    <ul class="meta-post">
                        <li><i class="fa fa-clock-o"></i> <span>{{ date('F d, Y G:i', strtotime($category->created_at)) }}</span></li>
                    </ul>
                </div>
                <p>{{ $category->description }}</p>

                <div class="spacing"></div>

                @endforeach
            </div><!--/col-lg-8 -->


            <!-- SIDEBAR -->
            <div class="col-lg-4">
                @include ('layouts.sidebar')
            </div>
        </div><!--/row -->
    </div><!--/container -->
@endsection