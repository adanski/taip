@extends('master')
@section('title')
    Dodawanie artykułu
@endsection
@section('content')
    <div class="container mtb">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="card">
                <div class="panel-body">
                    <!-- Formularz -->
                    {!! Form::open(['route' => 'articles.index', 'files' => true, 'class' => 'form-horizontal']) !!}

                        @include('articles.form', ['sendText' => 'Dodaj artykuł'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection