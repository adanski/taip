<?php

namespace Taip\Http\Controllers;

use Illuminate\Http\Request;
use Taip\Mail\ContactFormSent;
use Validator;
use Session;
use Mail;

class PagesController extends Controller
{
    public function contact() {
        return view('pages.contact');
    }

    public function contactMessage(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'max:255|alpha_dash',
            'email' => 'required|email|max:255',
            'subject' => 'required|max:255',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('contact')
                ->withErrors($validator)
                ->withInput();
        }

        Mail::to('admin@taip.com')->send(new ContactFormSent($request));

        Session::flash('contact_messaged', 'Wiadomość została wysłana.');
        return redirect()->route('contact');
    }

    public function about() {
        return view('pages.about');
    }

}
