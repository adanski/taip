@extends('master')
@section('title')
    Kontakt
@endsection
@section('content')
    <div class="container mtb">
    <div class="row">
        <div class="col-lg-8">
            <h4>Pytania?</h4>
            <div class="hline"></div>
            <p>Jeżeli masz jakiekolwiek pytania dotyczące działalności strony, chcesz zgłosić błąd albo interesuje cię inna sprawa, nie wahaj się skorzystać z poniższego formularza.</p>
            @if (Session::has('contact_messaged'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('contact_messaged') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul style="float: none; list-style: none; margin: 0; padding: 0;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route' => 'contact', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Imię') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź imię']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Adres e-mail') !!}
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź e-mail', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('subject', 'Temat') !!}
                    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź temat', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('message', 'Wiadomość') !!}
                    {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Wprowadź treść wiadomości', 'rows' => '3', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::button('<i class="fa fa-share-square-o" aria-hidden="true"></i> Wyślij', ['type' => 'submit', 'class' => 'btn btn-theme']) !!}
                </div>

            {!! Form::close() !!}
        </div><!--/col-lg-8 -->

        <div class="col-lg-4">
            <h4>Nasza siedziba</h4>
            <div class="hline"></div>
            <p>
                ul. Nadbystrzycka 99<br/>
                99-999 Lublin<br/>
                Polska<br/>
            </p>
            <p>
                Email: kontakt@taip.pl<br/>
                Tel: +48 999-998-997
            </p>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
    </div><!--/row -->
    </div>
@endsection