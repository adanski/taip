// make sure jQuery was loaded before taip.js
if (typeof jQuery === 'undefined') {
    throw new Error('TAIP requires jQuery');
}
else {

// $(document).ready() block
    $(function () {
        // PRETTYPHOTO PART
        $('a[data-gal]').each(function () {
            $(this).attr('rel', $(this).data('gal'));
        });
        $("a[data-rel^='prettyPhoto']").prettyPhoto({
            animationSpeed: 'slow',
            theme: 'light_square',
            slideshow: false,
            overlay_gallery: false,
            social_tools: false,
            deeplinking: false
        });
        // END PRETTYPHOTO PART

        // JQUERY-COMMENTS PART

        if (typeof TaipMeta === 'undefined' || !TaipMeta) return;
        $('#comments').comments({
            // current user
            currentUserId: TaipMeta.currentUserId,
            profilePictureURL: 'https://app.viima.com/static/media/user_profiles/user-icon.png',
            youText: TaipMeta.currentUserName,

            // functionalities
            enableUpvoting: false,
            enableDeleting: false,
            enableEditing: true,
            enableDeletingCommentWithReplies: false,
            enableAttachments: false,
            enablePinging: false,

            // settings
            defaultNavigationSortKey: 'oldest',
            maxRepliesVisible: -1,

            fieldMappings: {
                id: 'id',
                parent: 'parent_id',
                created: 'created_at',
                modified: 'updated_at',
                content: 'content',
                file: '',
                fileURL: '',
                fileMimeType: '',
                pings: '',
                creator: '',
                fullname: 'user_name',
                profileId: 'user_id',
                profileURL: '',
                profilePictureURL: '',
                createdByAdmin: '',
                createdByCurrentUser: '',
                upvoteCount: '',
                userHasUpvoted: '',
            },

            getComments: function (success, error) {
                // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
                let request = new Request(TaipMeta.commentsRouteUrl, {
                    method: 'GET',
                    credentials: 'same-origin',
                    mode: 'same-origin',
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'Accept': 'application/json; charset=UTF-8',
                    }
                });
                fetch(request)
                    .then((response) => {
                        if (!response.ok) {
                            throw new Error('Network failure occured.');
                        }
                        let body = response.json();
                        return body;
                    })
                    .then((body) => {
                        success(body);
                    })
                    .catch((error) => {
                        error();
                        console.log('getComments failed: ', error.message);
                    });
            },

            postComment: function (commentJSON, success, error) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    contentType: "application/json; charset=UTF-8",
                    url: TaipMeta.commentsRouteUrl,
                    data: JSON.stringify(commentJSON),
                    dataType: "json",
                    success: function (comment) {
                        success(comment);
                    },
                    error: function (errorText) {
                        error();
                        console.log('ERROR: ', errorText);
                    }
                });
            },

            putComment: function (commentJSON, success, error) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "PUT",
                    contentType: "application/json; charset=UTF-8",
                    url: TaipMeta.commentsRouteUrl + '/' + commentJSON.id,
                    data: JSON.stringify(commentJSON),
                    dataType: "json",
                    success: function (comment) {
                        success(comment);
                    },
                    error: function (errorText) {
                        error();
                        console.log('ERROR: ', errorText);
                    }
                });
            }
        });
        // END JQUERY-COMMENTS PART
    });
}