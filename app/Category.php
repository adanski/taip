<?php

namespace Taip;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'short_name', 'description',
    ];

    /**
     * Get category's articles.
     */
    public function articles() {
        return $this->hasMany(Article::class);
    }
}
