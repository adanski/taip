@extends('master')
@section('title')
    Resetowanie hasła
@endsection
@section('content')
    <div class="container mtb">
        <div class="row row-centered">
            <div class="col-md-5">
                <div class="card">
                    <div class="panel-body">
                        {!! Form::open(['url' => '/password/email', 'class' => 'form-horizontal']) !!}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul style="float: none; list-style: none; margin: 0; padding: 0;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="form-group">
                            {!! Form::label('email', 'Adres e-mail', ['class' => 'cols-sm-2 control-label']) !!}
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Wprowadź e-mail', 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Wyślij link resetujący</button>
                        </div>
                        <div class="login-register">
                            Pamiętasz hasło? <a href="{{ URL::route('login') }}">Zaloguj się</a>.
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection