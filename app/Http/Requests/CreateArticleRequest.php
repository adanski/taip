<?php

namespace Taip\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*if ($this->isMethod('PATCH')) {
            $article = Article::find($this->route('article'))
        }*/
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'intro_content' => 'required',
            'full_content' => 'required',
            'category_id' => 'required|integer|min:1',
            'img' => 'image|dimensions:min_width=850,min_height=50,max_height=400',
        ];
    }
}
