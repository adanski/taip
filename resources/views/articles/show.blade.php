@extends('master')
@section('title')
    {{ $article->title }}
@endsection
@section('article_path')
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('home') }}"><i class="fa fa-home"></i></a></li>
        <li>
            <a href="{{--{{ URL::route('categories.show', $article->category_id) }}--}}">{{ $article->category->name }}</a>
        </li>
        <li class="active">{{ $article->title }}</li>
    </ol>
@endsection
@section('content')
    <div class="container mtb">
        @if (Session::has('article_edited'))
            <div class="alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('article_edited') }}
            </div>
        @endif
        <div class="row">

            <!-- SINGLE POST -->
            <div class="col-lg-8">
                <!-- Blog Post 1 -->
                <p><img class="img-responsive" src="{{ URL::asset(Storage::url($article->img)) }}"></p>
                <h3 class="ctitle"></h3>
                <p>{{ $article->intro_content }}</p>
                <p>{{ $article->full_content }}</p>

                <div class="bottom-article">
                    <ul class="meta-post">
                        <li><i class="fa fa-clock-o"></i>
                            <span>{{ date('F d, Y G:i', strtotime($article->created_at)) }}</span></li>
                        <li><i class="fa fa-user"></i><a href="#">{{ $article->user->name }}</a></li>
                        <li><i class="fa fa-comments"></i><a href="#comments">{{ $commentsCount }} komentarzy</a></li>
                        <li>
                            <i class="fa fa-tags"></i>
                            <a href="{{--{{ URL::route('categories.show', $article->category_id) }}--}}">{{ $article->category->name }}</a>
                        </li>
                    </ul>
                </div>

                @can ('update', $article)
                    <div style="margin: 10px 0;">
                        <a href="{{ URL::route('articles.edit', $article->id) }}" class="btn btn-theme">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edytuj
                        </a>
                        @can ('delete', $article)
                            <a href="{{ URL::route('articles.index') }}" class="btn btn-theme"
                               onclick="event.preventDefault();
                                    document.getElementById('article_delete_form').submit();">
                                <i class="fa fa-trash-o" aria-hidden="true"></i> Usuń
                            </a>

                            {!! Form::open(['route' => ['articles.destroy', $article->id], 'method' => 'DELETE', 'style' => 'display: none;', 'id' => 'article_delete_form']) !!}
                            {!! Form::close() !!}
                        @endcan
                    </div>
                @endcan

                <div class="comment-area" id="comments">
                    {{-- jquery-comments.js plugin is injected here --}}
                </div>

            </div><!--/col-lg-8 -->

            <!-- SIDEBAR -->
            <div class="col-lg-4">
                @include ('layouts.sidebar')
            </div>
        </div><!--/row -->
    </div><!--/container -->
@endsection
@section ('customJs')
    @php
        $resourceId = $article->id;
    @endphp
    <script>
        @if (isset($resourceId) && $resourceId != null)
            var TaipMeta = {
                commentsRouteUrl: "{{ URL::route('comments.index', $resourceId) }}",
            };
        @endif
        @if (Auth::guest())
            TaipMeta.currentUserName = null;
        @else
            TaipMeta.currentUserName = "{{ Auth::user()->name }}";
            TaipMeta.currentUserId = "{{ Auth::id() }}";
        @endif
    </script>
    <script src="{{ URL::asset('js/jquery-comments.js') }}"></script>
    <script src="{{ URL::asset('js/app.js') }}"></script>
@endsection